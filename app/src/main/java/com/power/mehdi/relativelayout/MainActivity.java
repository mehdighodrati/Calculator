package com.power.mehdi.relativelayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnAdd;
    Button btnSub;
    Button btnMul;
    Button btnDiv;
    Button btnPoint;
    Button btnCe;
    Button btnClear;
    Button btnBack;
    Button btnSign;
    Button btnEqual;

    TextView txt1;
    TextView txt2;

    private String number1;
    private String number2;
    private int operation=0;


    public int add(int x, int y) {

        return x+y;
    }
    public int sub(int x, int y) {
        return x-y;
    }
    public int mul(int x, int y) {
        return x * y;
    }
    public int div(int x, int y){
        return x/y;
    }

    private boolean clearText = false;

    public int operand() {




        return 0;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt1 = findViewById(R.id.txt1);
        txt2 = findViewById(R.id.txt2);


        btn0 = findViewById(R.id.button0);
        btn1 = findViewById(R.id.button1);
        btn2 = findViewById(R.id.button2);
        btn3 = findViewById(R.id.button3);
        btn4 = findViewById(R.id.button4);
        btn5 = findViewById(R.id.button5);
        btn6 = findViewById(R.id.button6);
        btn7 = findViewById(R.id.button7);
        btn8 = findViewById(R.id.button8);
        btn9 = findViewById(R.id.button9);
        btnAdd = findViewById(R.id.buttonAdd);
        btnSub = findViewById(R.id.buttonSub);
        btnMul = findViewById(R.id.buttonMultiply);
        btnDiv = findViewById(R.id.buttonDivision);
        btnSign = findViewById(R.id.buttonSign);
        btnSign = findViewById(R.id.buttonSign);
        btnCe = findViewById(R.id.buttonCE);
        btnClear = findViewById(R.id.buttonC);
        btnBack = findViewById(R.id.buttonBack);
        btnEqual = findViewById(R.id.buttonEqual);



//        View.OnClickListener listener = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        };

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                txt2.setText(txt2.getText().toString()+"0");
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"1");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt2.getText().toString().equals("0") || clearText == true) {
                    txt2.setText("");
                }
                clearText = false;
                txt2.setText(txt2.getText().toString()+"9");
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt2.setText("");
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int x = Integer.parseInt(txt2.getText().toString());
                x = x/10;
                txt2.setText(String.valueOf(x));
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number1 = txt2.getText().toString();

                txt1.setText(txt1.getText().toString()+txt2.getText().toString()+"+");

                clearText = true;
                operation = 1;


            }
        });
        btnSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number1 = txt2.getText().toString();

                txt1.setText(txt1.getText().toString()+txt2.getText().toString()+"-");

                clearText = true;
                operation = 2;


            }
        });
        btnMul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number1 = txt2.getText().toString();

                txt1.setText(txt1.getText().toString()+txt2.getText().toString()+"*");

                clearText = true;
                operation = 3;


            }
        });
        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number1 = txt2.getText().toString();

                txt1.setText(txt1.getText().toString()+txt2.getText().toString()+"/");

                clearText = true;
                operation = 4;


            }
        });
        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number2 = txt2.getText().toString();
                txt1.setText("");
                switch (operation) {
                    case 1:
                        txt2.setText( add(Integer.parseInt(number1) ,Integer.parseInt(number2) ) + "");
                        operation = 0;
                        break;
                    case 2:
                        txt2.setText( sub(Integer.parseInt(number1) ,Integer.parseInt(number2) ) + "");
                        operation = 0;
                        break;
                    case 3:
                        txt2.setText( mul(Integer.parseInt(number1) ,Integer.parseInt(number2) ) + "");
                        operation = 0;
                        break;
                    case 4:
                        txt2.setText( div(Integer.parseInt(number1) ,Integer.parseInt(number2) ) + "");
                        operation = 0;
                    default:
                        txt2.setText(txt2.getText()+"");
                        break;
                }


            }
        });


    }



    public void onClickNumbers (View v ) {


    }
}
